#!/bin/bash

#Date;
#Numéro d'opération;
#Libellé;
#Débit;
#Crédit;
#Détail;


echo Hello

echo "[" > $1.json

while IFS=";" read a b c d e f x; do

    if [[ -n $d ]]; then 
      ammount=$(echo $d | tr , .)
      credit="false"
    else
      ammount=$(echo $e | tr , .| tr "\+" " ")
      credit="true"
    fi

    echo "  {" >> $1.json  
    echo "     \"id\": \"$b\", " >> $1.json  
    echo "     \"title\": \"$c\", " >> $1.json  
    echo "     \"date\": \"$a\", " >> $1.json  
    echo "     \"ammount\": ${ammount}, " >> $1.json  
    echo "     \"credit\": $credit, " >> $1.json  
    echo "     \"details\": \"$f\" " >> $1.json  
    echo "  }," >> $1.json  
done < $1

echo "  {" >> $1.json  
    echo "     \"id\": \"0110201520151001-21.13.06.358229 -x\"," >> $1.json  
    echo "     \"title\": \"INTERETS DEBITEURS\"," >> $1.json  
    echo "     \"date\": \"01/10/15\"," >> $1.json  
    echo "     \"ammount\": 3, " >> $1.json  
    echo "     \"credit\": false," >> $1.json  
    echo "     \"details\": \"INTERETS CREDITEURS \"" >> $1.json  
    echo "  }" >> $1.json  

echo "]" >> $1.json
